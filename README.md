# Loaf programming language.

TODO:

-   ~~move to gitlab~~
-   ~~rewrite lexer to remove `regex` and `lazy-static` dependencies~~
-   ~~try and make parser types more token specific/accurate~~
-   ~~convert to tabs~~
-   ~~name the binary loafc~~
-   ~~update ast~~
-   rewrite in mulitple languages:
    -   loaf
        -   ~~loaf-rust~~
        -   loaf-zig
        -   loaf-c
        -   loaf-odin

## W3C EBNF

Rialroad Diagram Generator: https://bottlecaps.de/rr/ui

```
StatementList           ::=   Statement*
Statement               ::=   Expr ';'
Expr                    ::=   AdditiveExpr
AdditiveExpr            ::=   MultiplicativeExpr ( AdditiveOperator MultiplicativeExpr )*
AdditiveOperator        ::=   '+'
                           |  '-'
MultiplicativeExpr      ::=   UnaryExpr ( MultiplicativeOperator UnaryExpr )*
MultiplicativeOperator  ::=   '*'
                           |  '/'
UnaryExpr               ::=   UnaryOperator? PrimaryExpr
UnaryOperator           ::=   '+'
                           |  '-'
PrimaryExpr             ::=   IntegerLiteral
                           |  ParenthesizedExpr
IntegerLiteral          ::=   [0-9]+
ParenthesizedExpr       ::=   '(' Expr ')'
```

## Augment production grammar

This, along with the [Canonical LR(0) items](#canonical-lr0-items) could be for writing a bottom-up parser, such as an SLR parser. Honestly though, it may just be too unwieldy to write manually.

```
Program                 -> .StatementList
StatementList           -> .Statement StatementList
StatementList           -> .𝜺
Statement               -> .Expr ';'
Expr                    -> .AdditiveExpr
AdditiveExpr            -> .MultiplicativeExpr AdditiveExprs
AdditiveExpr            -> .MultiplicativeExpr
AdditiveExprs           -> .AdditiveOperator MultiplicativeExpr AdditiveExprs
AdditiveExprs           -> .AdditiveOperator MultiplicativeExpr
AdditiveOperator        -> .'+'
AdditiveOperator        -> .'-'
MultiplicativeExpr      -> .UnaryExpr MultiplicativeExprs
MultiplicativeExpr      -> .UnaryExpr
MultiplicativeExprs     -> .MultiplicativeOperator UnaryExpr MultiplicativeExprs
MultiplicativeExprs     -> .MultiplicativeOperator UnaryExpr
MultiplicativeOperator  -> .'*'
MultiplicativeOperator  -> .'/'
UnaryExpr               -> .UnaryOperator PrimaryExpr
UnaryExpr               -> .PrimaryExpr
UnaryOperator           -> .'+'
UnaryOperator           -> .'-'
PrimaryExpr             -> .IntegerLiteral
PrimaryExpr             -> .ParenthesizedExpr
IntegerLiteral          -> .int
ParenthesizedExpr       -> .'(' Expr ')'
```

## Canonical LR(0) items

```
i0 = closure(Program -> .StatementList)
   = Program            -> .StatementList
     StatementList      -> .Statement StatementList
     StatementList      -> .𝜺
     Statement          -> .Expr ';'
     Expr               -> .AdditiveExpr
     AdditiveExpr       -> .MultiplicativeExpr AdditiveExprs
     AdditiveExpr       -> .MultiplicativeExpr
     MultiplicativeExpr -> .UnaryExpr MultiplicativeExprs
     MultiplicativeExpr -> .UnaryExpr
     UnaryExpr          -> .UnaryOperator PrimaryExpr
     UnaryExpr          -> .PrimaryExpr
     UnaryOperator      -> .'+'
     UnaryOperator      -> .'-'
     PrimaryExpr        -> .IntegerLiteral
     PrimaryExpr        -> .ParenthesizedExpr
     IntegerLiteral     -> .int
     ParenthesizedExpr  -> .'(' Expr ')'

//================ i0 Productions ================//

i1 = goto(i0, StatementList)
   = closure(Program -> StatementList.)
   = Program -> StatementList.

i2 = goto(i0, Statement)
   = closure(StatementList -> Statement. StatementList)
   = StatementList      -> Statement. StatementList
     StatementList      -> .Statement StatementList
     StatementList      -> .𝜺
     Statement          -> .Expr ';'
     Expr               -> .AdditiveExpr
     AdditiveExpr       -> .MultiplicativeExpr AdditiveExprs
     AdditiveExpr       -> .MultiplicativeExpr
     MultiplicativeExpr -> .UnaryExpr MultiplicativeExprs
     MultiplicativeExpr -> .UnaryExpr
     UnaryExpr          -> .UnaryOperator PrimaryExpr
     UnaryExpr          -> .PrimaryExpr
     UnaryOperator      -> .'+'
     UnaryOperator      -> .'-'
     PrimaryExpr        -> .IntegerLiteral
     PrimaryExpr        -> .ParenthesizedExpr
     IntegerLiteral     -> .int
     ParenthesizedExpr  -> .'(' Expr ')'

i3 = goto(i0, 𝜺)
   = closure(StatementList -> 𝜺.)
   = StatementList -> 𝜺.

i4 = goto(i0, Expr)
   = closure(Statement -> Expr. ';')
   = Statement -> Expr. ';'

i5 = goto(i0, AdditiveExpr)
   = closure(Expr -> AdditiveExpr.)
   = Expr -> AdditiveExpr.

i6 = goto(i0, MultiplicativeExpr)
   = closure(
        AdditiveExpr -> MultiplicativeExpr. AdditiveExprs
        AdditiveExpr -> MultiplicativeExpr.
    )
   = AdditiveExpr     -> MultiplicativeExpr. AdditiveExprs
     AdditiveExpr     -> MultiplicativeExpr.
     AdditiveExprs    -> .AdditiveOperator MultiplicativeExpr AdditiveExprs
     AdditiveExprs    -> .AdditiveOperator MultiplicativeExpr
     AdditiveOperator -> .'+'
     AdditiveOperator -> .'-'

i7 = goto(i0, UnaryExpr)
   = closure(
        MultiplicativeExpr -> UnaryExpr. MultiplicativeExprs
        MultiplicativeExpr -> UnaryExpr.
    )
   = MultiplicativeExpr     -> UnaryExpr. MultiplicativeExprs
     MultiplicativeExpr     -> UnaryExpr.
     MultiplicativeExprs    -> .MultiplicativeOperator UnaryExpr MultiplicativeExprs
     MultiplicativeExprs    -> .MultiplicativeOperator UnaryExpr
     MultiplicativeOperator -> .'*'
     MultiplicativeOperator -> .'/'

i8 = goto(i0, UnaryOperator)
   = closure(UnaryExpr -> UnaryOperator. PrimaryExpr)
   = UnaryExpr         -> UnaryOperator. PrimaryExpr
     PrimaryExpr       -> .IntegerLiteral
     PrimaryExpr       -> .ParenthesizedExpr
     IntegerLiteral    -> .int
     ParenthesizedExpr -> .'(' Expr ')'

i9 = goto(i0, PrimaryExpr)
   = closure(UnaryExpr -> PrimaryExpr.)
   = UnaryExpr -> PrimaryExpr.

i10 = goto(i0, '+')
    = closure(UnaryOperator -> '+'.)
    = UnaryOperator -> '+'.

i11 = goto(i0, '-')
    = closure(UnaryOperator -> '-'.)
    = UnaryOperator -> '-'.

i12 = goto(i0, IntegerLiteral)
    = closure(PrimaryExpr -> IntegerLiteral.)
    = PrimaryExpr -> IntegerLiteral.

i13 = goto(i0, ParenthesizedExpr)
    = closure(PrimaryExpr -> ParenthesizedExpr.)
    = PrimaryExpr -> ParenthesizedExpr.

i14 = goto(i0, int)
    = closure(IntegerLiteral -> int.)
    = IntegerLiteral -> int.

i15 = goto(i0, '(')
    = closure(ParenthesizedExpr -> '('. Expr ')')
    = ParenthesizedExpr  -> '('. Expr ')'
      Expr               -> .AdditiveExpr
      AdditiveExpr       -> .MultiplicativeExpr AdditiveExprs
      AdditiveExpr       -> .MultiplicativeExpr
      MultiplicativeExpr -> .UnaryExpr MultiplicativeExprs
      MultiplicativeExpr -> .UnaryExpr
      UnaryExpr          -> .UnaryOperator PrimaryExpr
      UnaryExpr          -> .PrimaryExpr
      UnaryOperator      -> .'+'
      UnaryOperator      -> .'-'
      PrimaryExpr        -> .IntegerLiteral
      PrimaryExpr        -> .ParenthesizedExpr
      IntegerLiteral     -> .int
      ParenthesizedExpr  -> .'(' Expr ')'

//================ i2 Productions ================//

i16 = goto(i2, StatementList)
    = closure(StatementList -> Statement StatementList.)
    = StatementList -> Statement StatementList.

i17 = goto(i2, Statement)
    = closure(StatementList -> Statement. StatementList)
    = i2

i18 = goto(i2, 𝜺)
    = closure(StatementList -> 𝜺.)
    = i3

i19 = goto(i2, Expr)
    = closure(Statement -> Expr. ';')
    = i4

i20 = goto(i2, AdditiveExpr)
    = closure(Expr -> AdditiveExpr.)
    = i5

i21 = goto(i2, MultiplicativeExpr)
    = closure(
        AdditiveExpr -> MultiplicativeExpr. AdditiveExprs
        AdditiveExpr -> MultiplicativeExpr.
    )
    = i6

i22 = goto(i2, UnaryExpr)
    = closure(
        MultiplicativeExpr -> UnaryExpr. MultiplicativeExprs
        MultiplicativeExpr -> UnaryExpr.
    )
    = i7

i23 = goto(i2, UnaryOperator)
    = closure(UnaryExpr -> UnaryOperator. PrimaryExpr)
    = i8

i24 = goto(i2, PrimaryExpr)
    = closure(UnaryExpr -> PrimaryExpr.)
    = i9

i25 = goto(i2, '+')
    = closure(UnaryOperator -> '+'.)
    = i10

i26 = goto(i2, '-')
    = closure(UnaryOperator -> '-'.)
    = i11

i27 = goto(i2, IntegerLiteral)
    = closure(PrimaryExpr -> IntegerLiteral.)
    = i12

i28 = goto(i2, ParenthesizedExpr)
    = closure(PrimaryExpr -> ParenthesizedExpr.)
    = i13

i29 = goto(i2, int)
    = closure(IntegerLiteral -> int.)
    = i14

i30 = goto(i2, '(')
    = closure(ParenthesizedExpr -> '('. Expr ')')
    = i15

//================ i4 Productions ================//

i31 = goto(i4, ';')
    = closure(Statement -> Expr ';'.)
    = Statement -> Expr ';'.

//================ i6 Productions ================//

i32 = goto(i6, AdditiveExprs)
    = closure(AdditiveExpr -> MultiplicativeExpr AdditiveExprs.)
    = AdditiveExpr -> MultiplicativeExpr AdditiveExprs.

i33 = goto(i6, AdditiveOperator)
    = closure(
        AdditiveExprs -> AdditiveOperator. MultiplicativeExpr AdditiveExprs
        AdditiveExprs -> AdditiveOperator. MultiplicativeExpr
    )
    = AdditiveExprs      -> AdditiveOperator. MultiplicativeExpr AdditiveExprs
      AdditiveExprs      -> AdditiveOperator. MultiplicativeExpr
      MultiplicativeExpr -> .UnaryExpr MultiplicativeExprs
      MultiplicativeExpr -> .UnaryExpr
      UnaryExpr          -> .UnaryOperator PrimaryExpr
      UnaryExpr          -> .PrimaryExpr
      UnaryOperator      -> .'+'
      UnaryOperator      -> .'-'
      PrimaryExpr        -> .IntegerLiteral
      PrimaryExpr        -> .ParenthesizedExpr
      IntegerLiteral     -> .int
      ParenthesizedExpr  -> .'(' Expr ')'

i34 = goto(i6, '+')
    = closure(AdditiveOperator -> '+'.)
    = AdditiveOperator -> '+'.

i35 = goto(i6, '-')
    = closure(AdditiveOperator -> '-'.)
    = AdditiveOperator -> '-'.

//================ i7 Productions ================//

i36 = goto(i7, MultiplicativeExprs)
    = closure(MultiplicativeExpr -> UnaryExpr MultiplicativeExprs.)
    = MultiplicativeExpr -> UnaryExpr MultiplicativeExprs.

i37 = goto(i7, MultiplicativeOperator)
    = closure(
        MultiplicativeExprs -> MultiplicativeOperator. UnaryExpr MultiplicativeExprs
        MultiplicativeExprs -> MultiplicativeOperator. UnaryExpr
    )
    = MultiplicativeExprs -> MultiplicativeOperator. UnaryExpr MultiplicativeExprs
      MultiplicativeExprs -> MultiplicativeOperator. UnaryExpr
      UnaryExpr           -> .UnaryOperator PrimaryExpr
      UnaryExpr           -> .PrimaryExpr
      UnaryOperator       -> .'+'
      UnaryOperator       -> .'-'
      PrimaryExpr         -> .IntegerLiteral
      PrimaryExpr         -> .ParenthesizedExpr
      IntegerLiteral      -> .int
      ParenthesizedExpr   -> .'(' Expr ')'

i38 = goto(i7, '*')
    = closure(MultiplicativeOperator -> '*'.)
    = MultiplicativeOperator -> '*'.

i39 = goto(i7, '/')
    = closure(MultiplicativeOperator -> '/'.)
    = MultiplicativeOperator -> '/'.

//================ i8 Productions ================//

i40 = goto(i8, PrimaryExpr)
    = closure(UnaryExpr -> PrimaryExpr.)
    = i9

i41 = goto(i8, IntegerLiteral)
    = closure(PrimaryExpr -> IntegerLiteral.)
    = i12

i42 = goto(i8, ParenthesizedExpr)
    = closure(PrimaryExpr -> ParenthesizedExpr.)
    = i13

i43 = goto(i8, int)
    = closure(IntegerLiteral -> int.)
    = i14

i44 = goto(i8, '(')
    = closure(ParenthesizedExpr -> '('. Expr ')')
    = i15

//================ i15 Productions ================//

i45 = goto(i15, Expr)
    = closure(ParenthesizedExpr -> '(' Expr. ')')
    = ParenthesizedExpr -> '(' Expr. ')'

i46 = goto(i15, AdditiveExpr)
    = closure(Expr -> AdditiveExpr.)
    = i5

i47 = goto(i15, MultiplicativeExpr)
    = closure(
        AdditiveExpr -> MultiplicativeExpr. AdditiveExprs
        AdditiveExpr -> MultiplicativeExpr.
    )
    = i6

i48 = goto(i15, UnaryExpr)
    = closure(
        MultiplicativeExpr -> UnaryExpr. MultiplicativeExprs
        MultiplicativeExpr -> UnaryExpr.
    )
    = i7

i49 = goto(i15, UnaryOperator)
    = closure(UnaryExpr -> UnaryOperator. PrimaryExpr)
    = i8

i50 = goto(i15, PrimaryExpr)
    = closure(UnaryExpr -> PrimaryExpr.)
    = i9

i51 = goto(i15, '+')
    = closure(UnaryOperator -> '+'.)
    = i10

i52 = goto(i15, '-')
    = closure(UnaryOperator -> '-'.)
    = i11

i53 = goto(i15, IntegerLiteral)
    = closure(PrimaryExpr -> IntegerLiteral.)
    = i12

i54 = goto(i15, ParenthesizedExpr)
    = closure(PrimaryExpr -> ParenthesizedExpr.)
    = i13

i55 = goto(i15, int)
    = closure(IntegerLiteral -> int.)
    = i14

i56 = goto(i15, '(')
    = closure(ParenthesizedExpr -> '('. Expr ')')
    = i15

//================ i33 Productions ================//

i57 = goto(i33, MultiplicativeExpr)
    = closure(
        AdditiveExprs -> AdditiveOperator MultiplicativeExpr. AdditiveExprs
        AdditiveExprs -> AdditiveOperator MultiplicativeExpr.
    )
    = AdditiveExprs    -> AdditiveOperator MultiplicativeExpr. AdditiveExprs
      AdditiveExprs    -> AdditiveOperator MultiplicativeExpr.
      AdditiveExprs    -> .AdditiveOperator MultiplicativeExpr AdditiveExprs
      AdditiveExprs    -> .AdditiveOperator MultiplicativeExpr
      AdditiveOperator -> .'+'
      AdditiveOperator -> .'-'

i58 = goto(i33, UnaryExpr)
    = closure(
        MultiplicativeExpr -> UnaryExpr. MultiplicativeExprs
        MultiplicativeExpr -> UnaryExpr.
    )
    = i7

i59 = goto(i33, UnaryOperator)
    = closure(UnaryExpr -> UnaryOperator. PrimaryExpr)
    = i8

i60 = goto(i33, PrimaryExpr)
    = closure(UnaryExpr -> PrimaryExpr.)
    = i9

i61 = goto(i33, '+')
    = closure(UnaryOperator -> '+'.)
    = i10

i62 = goto(i33, '-')
    = closure(UnaryOperator -> '-'.)
    = i11

i63 = goto(i33, IntegerLiteral)
    = closure(PrimaryExpr -> IntegerLiteral.)
    = i12

i64 = goto(i33, ParenthesizedExpr)
    = closure(PrimaryExpr -> ParenthesizedExpr.)
    = i13

i65 = goto(i33, int)
    = closure(IntegerLiteral -> int.)
    = i14

i66 = goto(i33, '(')
    = closure(ParenthesizedExpr -> '('. Expr ')')
    = i15

//================ i37 Productions ================//

i67 = goto(i37, UnaryExpr)
    = closure(
        MultiplicativeExprs -> MultiplicativeOperator UnaryExpr. MultiplicativeExprs
        MultiplicativeExprs -> MultiplicativeOperator UnaryExpr.
    )
    = MultiplicativeExprs    -> MultiplicativeOperator UnaryExpr. MultiplicativeExprs
      MultiplicativeExprs    -> MultiplicativeOperator UnaryExpr.
      MultiplicativeExprs    -> .MultiplicativeOperator UnaryExpr MultiplicativeExprs
      MultiplicativeExprs    -> .MultiplicativeOperator UnaryExpr
      MultiplicativeOperator -> .'*'
      MultiplicativeOperator -> .'/'

i68 = goto(i37, UnaryOperator)
    = closure(UnaryExpr -> UnaryOperator. PrimaryExpr)
    = i8

i69 = goto(i37, PrimaryExpr)
    = closure(UnaryExpr -> PrimaryExpr.)
    = i9

i70 = goto(i37, '+')
    = closure(UnaryOperator -> '+'.)
    = i10

i71 = goto(i37, '-')
    = closure(UnaryOperator -> '-'.)
    = i11

i72 = goto(i37, IntegerLiteral)
    = closure(PrimaryExpr -> IntegerLiteral.)
    = i12

i73 = goto(i37, ParenthesizedExpr)
    = closure(PrimaryExpr -> ParenthesizedExpr.)
    = i13

i74 = goto(i37, int)
    = closure(IntegerLiteral -> int.)
    = i14

i75 = goto(i37, '(')
    = closure(ParenthesizedExpr -> '('. Expr ')')
    = i15

//================ i45 Productions ================//

i76 = goto(i45, ')')
    = closure(ParenthesizedExpr -> '(' Expr ')'.)
    = ParenthesizedExpr -> '(' Expr ')'.

//================ i57 Productions ================//

i77 = goto(i57, AdditiveExprs)
    = closure(AdditiveExprs -> AdditiveOperator MultiplicativeExpr AdditiveExprs.)
    = AdditiveExprs -> AdditiveOperator MultiplicativeExpr AdditiveExprs.

i78 = goto(i57, AdditiveOperator)
    = closure(
        AdditiveExprs -> AdditiveOperator. MultiplicativeExpr AdditiveExprs
        AdditiveExprs -> AdditiveOperator. MultiplicativeExpr
    )
    = i33

i79 = goto(i57, '+')
    = closure(AdditiveOperator -> '+'.)
    = i34

i80 = goto(i57, '-')
    = closure(AdditiveOperator -> '-'.)
    = i35

//================ i67 Productions ================//

i81 = goto(i67, MultiplicativeExprs)
    = closure(MultiplicativeExprs -> MultiplicativeOperator UnaryExpr MultiplicativeExprs.)
    = MultiplicativeExprs -> MultiplicativeOperator UnaryExpr MultiplicativeExprs.

i82 = goto(i67, MultiplicativeOperator)
    = closure(
        MultiplicativeExprs -> MultiplicativeOperator. UnaryExpr MultiplicativeExprs
        MultiplicativeExprs -> MultiplicativeOperator. UnaryExpr
    )
    = i37

i83 = goto(i67, '*')
    = closure(MultiplicativeOperator -> '*'.)
    = i38

i84 = goto(i67, '/')
    = closure(MultiplicativeOperator -> '/'.)
    = i39
```

---

TODO: general purpose programming language

Example:

```
fn main() {
    // stuff goes here
}
```

Some nearly Backus-Naur Form (BNF) nonsense:

```
Program = Function
Function = "fn" ID ( [Formal ⟦, Formal⟧*] ) : TYPE { ⟦Expr ;⟧* [Expr] }
Formal = ID : TYPE
Expr = "let" ID : TYPE = Expr
    | ID = Expr
    | ID ( [Expr ⟦, Expr⟧*] )
    | { ⟦Expr ;⟧* [Expr] }
    | ( Expr )
    | Expr + Expr
    | Expr - Expr
    | Expr * Expr
    | Expr / Expr
    | ID
    | INTEGER
    | STRING
    | "true"
    | "false"
```
