use crate::parser::{
	self, AdditiveExpr, AdditiveOperator, MultiplicativeExpr, MultiplicativeOperator, PrimaryExpr,
	UnaryExpr, UnaryOperator,
};

#[derive(Debug, Clone)]
pub struct StatementList(pub Vec<Statement>);

impl StatementList {
	pub fn new(parse_tree: &parser::StatementList) -> Self {
		Self(parse_tree.0.iter().map(Statement::new).collect())
	}
}

#[derive(Debug, Clone)]
pub struct Statement(pub Expr);

impl Statement {
	pub fn new(statement: &parser::Statement) -> Self {
		Self(Expr::from_expr(&statement.0))
	}
}

#[derive(Debug, Clone)]
pub enum Expr {
	Int(String),
	Add(Box<Expr>, Box<Expr>),
	Subtract(Box<Expr>, Box<Expr>),
	Negate(Box<Expr>),
	Multiply(Box<Expr>, Box<Expr>),
	Divide(Box<Expr>, Box<Expr>),
}

impl Expr {
	pub fn from_expr(expr: &parser::Expr) -> Self {
		match expr {
			parser::Expr::AdditiveExpr(additive_expr) => Self::from_additive_expr(additive_expr),
		}
	}
	fn from_additive_expr(additive_expr: &parser::AdditiveExpr) -> Self {
		match additive_expr {
			AdditiveExpr(multiplicative_expr, multiplicative_exprs) => {
				let expr = Self::from_multiplicative_expr(multiplicative_expr);
				multiplicative_exprs
					.iter()
					.fold(expr, |lhs, (op, multiplicative_expr)| {
						let rhs = Self::from_multiplicative_expr(multiplicative_expr);
						match op {
							AdditiveOperator::Plus => Self::Add(Box::new(lhs), Box::new(rhs)),
							AdditiveOperator::Minus => Self::Subtract(Box::new(lhs), Box::new(rhs)),
						}
					})
			}
		}
	}
	fn from_multiplicative_expr(multiplicative_expr: &parser::MultiplicativeExpr) -> Self {
		match multiplicative_expr {
			MultiplicativeExpr(unary_expr, unary_exprs) => {
				let expr = Self::from_unary_expr(unary_expr);
				unary_exprs.iter().fold(expr, |lhs, (op, unary_expr)| {
					let rhs = Self::from_unary_expr(unary_expr);
					match op {
						MultiplicativeOperator::Multiply => {
							Self::Multiply(Box::new(lhs), Box::new(rhs))
						}
						MultiplicativeOperator::Divide => {
							Self::Divide(Box::new(lhs), Box::new(rhs))
						}
					}
				})
			}
		}
	}
	fn from_unary_expr(unary_expr: &parser::UnaryExpr) -> Self {
		match unary_expr {
			UnaryExpr(unary_operator, primary_expr) => {
				let expr = Self::from_primary_expr(primary_expr);
				match unary_operator {
					Some(UnaryOperator::Minus) => Self::Negate(Box::new(expr)),
					_ => expr,
				}
			}
		}
	}
	fn from_primary_expr(primary_expr: &parser::PrimaryExpr) -> Self {
		match primary_expr {
			PrimaryExpr::Int(int) => Self::Int(int.clone()),
			PrimaryExpr::ParenthesizedExpr(parser::ParenthesizedExpr(expr)) => {
				Self::from_expr(expr)
			}
		}
	}
}
