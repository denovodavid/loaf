use crate::ast::{Expr, Statement, StatementList};

pub fn statement_list(sl: &StatementList) {
	sl.0.iter().for_each(statement)
}

pub fn statement(s: &Statement) {
	println!("{}", expr(&s.0))
}

pub fn expr(e: &Expr) -> i32 {
	match e {
		Expr::Int(value) => value.parse().unwrap(),
		Expr::Add(lhs, rhs) => expr(lhs) + expr(rhs),
		Expr::Subtract(lhs, rhs) => expr(lhs) - expr(rhs),
		Expr::Negate(rhs) => -expr(rhs),
		Expr::Multiply(lhs, rhs) => expr(lhs) * expr(rhs),
		Expr::Divide(lhs, rhs) => expr(lhs) / expr(rhs),
	}
}
