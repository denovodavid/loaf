#[derive(Debug, Clone)]
pub struct Lexer {
	source: String,
	cursor: usize,
}

impl Lexer {
	pub fn new(source: String) -> Self {
		Self { source, cursor: 0 }
	}
	fn next_token(&self) -> Option<(Token, usize)> {
		let mut chars = self.source[self.cursor..].chars();
		match chars.next() {
			// whitespace
			Some(c) if c.is_whitespace() => Some((
				Token::Whitespace,
				1 + chars.take_while(|c| c.is_whitespace()).count(),
			)),
			Some('/') => match chars.next() {
				// single line comment
				Some('/') => {
					let mut length: usize = 2;
					while let Some(c) = chars.next() {
						match c {
							'\r' => {
								length += match chars.next() {
									Some('\n') => 2,
									_ => 1,
								};
								break;
							}
							'\n' => {
								length += 1;
								break;
							}
							_ => length += 1,
						};
					}
					Some((Token::SingleLineComment, length))
				}
				// multi line comment
				Some('*') => {
					let mut length: usize = 2;
					while let Some(c) = chars.next() {
						match c {
							'*' if chars.next() == Some('/') => length += 2,
							_ => length += 1,
						}
					}
					Some((Token::MultiLineComment, length))
				}
				// forward slash
				Some(_) => Some((Token::ForwardSlash, 1)),
				_ => None,
			},
			// integer literal
			Some('0'..='9') => {
				let length = 1 + chars.take_while(|c| matches!(c, '0'..='9')).count();
				let value = self.source[self.cursor..(self.cursor + length)].to_owned();
				Some((Token::IntegerLiteral { value }, length))
			}
			// other tokens...
			Some('+') => Some((Token::Plus, 1)),
			Some('-') => Some((Token::Minus, 1)),
			Some('*') => Some((Token::Star, 1)),
			Some('(') => Some((Token::LeftParen, 1)),
			Some(')') => Some((Token::RightParen, 1)),
			Some(';') => Some((Token::Semicolon, 1)),
			_ => None,
		}
	}
	fn lex(&mut self) -> Option<Token> {
		if self.cursor < self.source.len() {
			let (token, length) = match self.next_token() {
				Some(value) => value,
				None => match &self.source[self.cursor..].chars().next() {
					Some(c) => panic!(
						"Invalid token, \"{}\" ({}), at position {}.",
						c,
						c.escape_unicode(),
						self.cursor
					),
					None => panic!("Unknown invalid token at position {}.", self.cursor),
				},
			};
			self.cursor += length;
			match token {
				Token::Whitespace | Token::SingleLineComment | Token::MultiLineComment => {
					self.lex()
				}
				_ => Some(token),
			}
		} else {
			None
		}
	}
}

impl Iterator for Lexer {
	type Item = Token;
	fn next(&mut self) -> Option<Self::Item> {
		self.lex()
	}
}

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
	Whitespace,
	SingleLineComment,
	MultiLineComment,
	IntegerLiteral { value: String },
	Plus,
	Minus,
	Star,
	ForwardSlash,
	LeftParen,
	RightParen,
	Semicolon,
}
