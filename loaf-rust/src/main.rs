mod ast;
mod interpreter;
mod lexer;
mod parser;

const SOURCE_CODE: &str = r###"
1 + -2 * 3; // -5
911;
100+1;
(((((1 + 1)))));
-1 + (2 + 3); // (-(1)) + (2 + 3) = 4
-1 * 2 + 3; // (-(1 * 2)) + 3 = 1
-1 + 2 * 3; // (-(1)) + (2 * 3) = 5
// ( ); // syntax error
// (*); // syntax error
// *+/-; // syntax error
"###;

fn main() {
	// let lexer = lexer::Lexer::new(SOURCE_CODE.to_string());
	// for token in lexer {
	// 	println!("{:#?}", token);
	// }
	let lexer = lexer::Lexer::new(SOURCE_CODE.to_string());
	let mut parser = parser::Parser::new(lexer);
	let parse_tree = parser.parse();
	println!("{:#?}", parse_tree);
	let ast = ast::StatementList::new(&parse_tree);
	println!("{:?}", ast);
	interpreter::statement_list(&ast);
}
