use crate::lexer::Token;
use std::iter::Peekable;

#[derive(Debug, Clone)]
pub struct StatementList(pub Vec<Statement>);

#[derive(Debug, Clone)]
pub struct Statement(pub Expr);

#[derive(Debug, Clone)]
pub enum Expr {
	AdditiveExpr(AdditiveExpr),
}

#[derive(Debug, Clone)]
pub struct AdditiveExpr(
	pub MultiplicativeExpr,
	pub Vec<(AdditiveOperator, MultiplicativeExpr)>,
);

#[derive(Debug, Clone)]
pub enum AdditiveOperator {
	Plus,
	Minus,
}

#[derive(Debug, Clone)]
pub struct MultiplicativeExpr(pub UnaryExpr, pub Vec<(MultiplicativeOperator, UnaryExpr)>);

#[derive(Debug, Clone)]
pub enum MultiplicativeOperator {
	Multiply,
	Divide,
}

#[derive(Debug, Clone)]
pub struct UnaryExpr(pub Option<UnaryOperator>, pub PrimaryExpr);

#[derive(Debug, Clone)]
pub enum UnaryOperator {
	Plus,
	Minus,
}

#[derive(Debug, Clone)]
pub enum PrimaryExpr {
	Int(String),
	ParenthesizedExpr(ParenthesizedExpr),
}

#[derive(Debug, Clone)]
pub struct ParenthesizedExpr(pub Box<Expr>);

/// Top down parser
#[derive(Debug, Clone)]
pub struct Parser<Iter>
where
	Iter: Iterator<Item = Token>,
{
	lexer: Peekable<Iter>,
}

impl<Iter> Parser<Iter>
where
	Iter: Iterator<Item = Token>,
{
	pub fn new(lexer: Iter) -> Self {
		Self {
			lexer: lexer.peekable(),
		}
	}
	pub fn parse(&mut self) -> StatementList {
		self.statement_list()
	}
	fn statement_list(&mut self) -> StatementList {
		let mut stmt_list = vec![];
		while let Some(stmt) = self.statement() {
			stmt_list.push(stmt);
		}
		if let Some(token) = self.lexer.peek() {
			panic!("Unexpected token, \"{:?}\", at position \"(TODO)\".", token,);
		}
		StatementList(stmt_list)
	}
	fn statement(&mut self) -> Option<Statement> {
		self.expr().map(|expr| {
			self.lexer
				.next_if_eq(&Token::Semicolon)
				.expect("Expected semicolon");
			Statement(expr)
		})
	}
	fn expr(&mut self) -> Option<Expr> {
		self.additive_expr().map(Expr::AdditiveExpr)
	}
	fn additive_expr(&mut self) -> Option<AdditiveExpr> {
		self.multiplicative_expr().map(|multiplicative_expr| {
			let mut additive_expr = AdditiveExpr(multiplicative_expr, vec![]);
			while let Some(op) = self.additive_operator() {
				let multiplicative_expr = self
					.multiplicative_expr()
					.expect("Expected multiplicative expr");
				additive_expr.1.push((op, multiplicative_expr));
			}
			additive_expr
		})
	}
	fn additive_operator(&mut self) -> Option<AdditiveOperator> {
		match self.lexer.peek() {
			Some(Token::Plus) => {
				self.lexer.next();
				Some(AdditiveOperator::Plus)
			}
			Some(Token::Minus) => {
				self.lexer.next();
				Some(AdditiveOperator::Minus)
			}
			_ => None,
		}
	}
	fn multiplicative_expr(&mut self) -> Option<MultiplicativeExpr> {
		self.unary_expr().map(|unary_expr| {
			let mut multiplicative_expr = MultiplicativeExpr(unary_expr, vec![]);
			while let Some(op) = self.multiplicative_operator() {
				let unary_expr = self.unary_expr().expect("Expected unary expr");
				multiplicative_expr.1.push((op, unary_expr));
			}
			multiplicative_expr
		})
	}
	fn multiplicative_operator(&mut self) -> Option<MultiplicativeOperator> {
		match self.lexer.peek() {
			Some(Token::Star) => {
				self.lexer.next();
				Some(MultiplicativeOperator::Multiply)
			}
			Some(Token::ForwardSlash) => {
				self.lexer.next();
				Some(MultiplicativeOperator::Divide)
			}
			_ => None,
		}
	}
	fn unary_expr(&mut self) -> Option<UnaryExpr> {
		self.unary_operator()
			.map(|unary_operator| {
				let primary_expr = self.primary_expr().expect("Expected primary expr");
				UnaryExpr(Some(unary_operator), primary_expr)
			})
			.or_else(|| {
				self.primary_expr()
					.map(|primary_expr| UnaryExpr(None, primary_expr))
			})
	}
	fn unary_operator(&mut self) -> Option<UnaryOperator> {
		match self.lexer.peek() {
			Some(Token::Plus) => {
				self.lexer.next();
				Some(UnaryOperator::Plus)
			}
			Some(Token::Minus) => {
				self.lexer.next();
				Some(UnaryOperator::Minus)
			}
			_ => None,
		}
	}
	fn primary_expr(&mut self) -> Option<PrimaryExpr> {
		self.int().map(PrimaryExpr::Int).or_else(|| {
			self.parenthesized_expr()
				.map(PrimaryExpr::ParenthesizedExpr)
		})
	}
	fn int(&mut self) -> Option<String> {
		match self.lexer.peek() {
			Some(Token::IntegerLiteral { value }) => {
				let value = value.clone();
				self.lexer.next();
				Some(value)
			}
			_ => None,
		}
	}
	fn parenthesized_expr(&mut self) -> Option<ParenthesizedExpr> {
		if let Some(Token::LeftParen) = self.lexer.peek() {
			self.lexer.next();
			let expr = self.expr().expect("Expected expr");
			self.lexer
				.next_if_eq(&Token::RightParen)
				.expect("Expected right parenthesis");
			Some(ParenthesizedExpr(Box::new(expr)))
		} else {
			None
		}
	}
}
