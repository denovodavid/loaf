const std = @import("std");
const print = std.debug.print;

pub fn main() !void {
    // NOTE(david): messing around with allocations
    // const arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    // const my_slice = arena.child_allocator.alloc(u8, 1);
    // if (my_slice != null) {
    //     my_slice[0] = 49;
    // }
    // print("{any}\n", .{my_slice});
    // arena.deinit();

    const source_code = "1 + -2 * (3); // -5";
    // const source_code = "";
    var lexer = Lexer{
        .source = source_code,
        .cursor = 0,
    };
    print("{}\n", .{lexer});
    while (true) {
        const tok = lexer.nextToken();
        if (tok) |t| print("{?}\n", .{t}) else break;
    }

    // for (source_code) |character, index| {
    // 	print("{c}\n", .{ character });
    // 	_ = index;
    // }
}

const TokenClass = enum {
    Whitespace,
    SingleLineComment,
    MultiLineComment,
    IntegerLiteral,
    Plus,
    Minus,
    Star,
    ForwardSlash,
    LeftParen,
    RightParen,
    Semicolon,
};

const Token = struct {
    class: TokenClass,
    value: []const u8,
};

const Lexer = struct {
    source: []const u8,
    cursor: usize,
    fn at(self: *Lexer, i: usize) ?u8 {
        if (i >= 0 and i < self.source.len) {
            return self.source[i];
        }
        return null;
    }
    fn nextToken(self: *Lexer) ?Token {
        const src = self.source;
        const len = src.len;
        if (self.cursor >= len) return null;
        const char = src[self.cursor];
        // print("{c}\n", .{char});
        var token: ?Token = null;
        if (std.ascii.isWhitespace(char)) {
            var end = self.cursor + 1;
            while (end < len and std.ascii.isWhitespace(src[end])) end += 1;
            token = .{
                .class = TokenClass.Whitespace,
                .value = src[self.cursor..end],
            };
        } else if (std.ascii.isDigit(char)) {
            var end = self.cursor + 1;
            while (end < len and std.ascii.isDigit(src[end])) end += 1;
            token = .{
                .class = TokenClass.IntegerLiteral,
                .value = src[self.cursor..end],
            };
        } else switch (char) {
            '/' => {
                var end = self.cursor + 1;
                switch (src[end]) {
                    // single line comment
                    '/' => {
                        end += 1;
                        while (end < len) {
                            switch (src[end]) {
                                '\r' => {
                                    end += 1;
                                    if (end < len and src[end] == '\n') end += 1;
                                    break;
                                },
                                '\n' => {
                                    end += 1;
                                    break;
                                },
                                else => end += 1,
                            }
                        }
                        token = .{
                            .class = TokenClass.SingleLineComment,
                            .value = src[self.cursor..end],
                        };
                    },
                    // multi line comment
                    '*' => {
                        end += 1;
                        while ((end + 1) < len and !(src[end] == '*' and src[end + 1] == '/')) end += 1;
                        token = .{
                            .class = TokenClass.MultiLineComment,
                            .value = src[self.cursor..end],
                        };
                    },
                    // forward slash
                    else => token = .{ .class = TokenClass.ForwardSlash, .value = "/" },
                }
            },
            '+' => token = .{ .class = TokenClass.Plus, .value = "+" },
            '-' => token = .{ .class = TokenClass.Minus, .value = "-" },
            '*' => token = .{ .class = TokenClass.Star, .value = "*" },
            '(' => token = .{ .class = TokenClass.LeftParen, .value = "(" },
            ')' => token = .{ .class = TokenClass.RightParen, .value = ")" },
            ';' => token = .{ .class = TokenClass.Semicolon, .value = ";" },
            else => token = null,
        }
        if (token) |t| self.cursor += t.value.len;
        return token;
    }
};
